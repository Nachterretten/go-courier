package cache

import (
	"github.com/go-redis/redis/v8"
)

func NewRedisClient(host, port string) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: "",
		DB:       0,
	})

	return client
}
