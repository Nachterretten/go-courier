package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"gitlab.com/ptflp/geotask/module/order/models"
	"log"
	"strconv"
	"time"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.2 --name=OrderStorager
type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int64) (*models.Order, error)                              // получить заказ по id
	GenerateUniqueID(ctx context.Context) (int64, error)                                            // сгенерировать уникальный id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int64, error)                                                    // получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error                                // удалить старые заказы по истечению времени maxAge
	SetOrderDelivered(ctx context.Context, order *models.Order) error                               // удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	storage *redis.Client
}

func NewOrderStorage(storage *redis.Client) OrderStorager {
	return &OrderStorage{storage: storage}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	return o.saveOrderWithGeo(ctx, order, maxAge)
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {

	// получаем интервал от нуля до текущего времени минус время жизни ордеров
	maxTime := fmt.Sprintf("%d", time.Now().UnixNano()-int64(maxAge))

	// получаем ID всех ордеров в указанном интервале, которые нужно удалить
	listId, err := o.storage.ZRangeByScore(ctx, "orders", &redis.ZRangeBy{Min: "-inf", Max: maxTime}).Result()
	if err != nil {
		return err
	}
	// проверяем срез на пустоту
	if len(listId) == 0 {
		return nil
	}

	// удаляем ордера из геоиндекса по их id
	for _, orderId := range listId {
		_, err := o.storage.ZRem(ctx, "geo_orders", orderId).Result()
		if err != nil {
			log.Printf("failed to delete order on geo_orders: %v", err)
		}
	}
	// удалять ордера по ключу не нужно, они будут удалены автоматически по истечению времени жизни

	// удаляем ордера из сортированного множества используя метод ZRemRangeByScore
	// max указывает максимальное значение score по заданному диапазону
	o.storage.ZRemRangeByScore(ctx, "orders", "-inf", maxTime)
	if err != nil {
		return err
	}
	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int64) (*models.Order, error) {
	var err error
	var data []byte
	var order models.Order

	// получаем ордер из redis по ключу order:ID
	key := fmt.Sprintf("order:%d", orderID)
	data, err = o.storage.Get(ctx, key).Bytes()
	// проверяем исключение redis.Nil, в этом случае возвращаем nil, nil
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	// десериализуем ордер из json
	err = json.Unmarshal(data, &order)
	if err != nil {
		return nil, err
	}

	return &order, nil
}

func (o *OrderStorage) saveOrderWithGeo(ctx context.Context, order models.Order, maxAge time.Duration) error {
	var err error
	var data []byte

	// сериализуем ордер в json
	data, err = json.Marshal(order)

	key := fmt.Sprintf("order:%d", order.ID)
	o.storage.Set(ctx, key, data, maxAge)

	o.storage.GeoAdd(ctx,
		"geo_orders",
		&redis.GeoLocation{
			Name:      fmt.Sprintf("%d", order.ID),
			Latitude:  order.Lat,
			Longitude: order.Lng,
		},
	)

	o.storage.ZAdd(ctx, "orders", &redis.Z{Score: float64(time.Now().UnixNano()), Member: order.ID})

	return err

}

func (o *OrderStorage) GetCount(ctx context.Context) (int64, error) {
	// получить количество ордеров в упорядоченном множестве используя метод ZCard
	count, err := o.storage.ZCard(ctx, "orders").Result()
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var err error
	var order *models.Order
	var orders []models.Order
	var ordersLocation []redis.GeoLocation

	// используем метод getOrdersByRadius для получения ID заказов в радиусе
	ordersLocation, err = o.getOrdersByRadius(ctx, lng, lat, radius, unit)
	// в случае отсутствия заказов в радиусе метод getOrdersByRadius должен вернуть nil, nil (при ошибке redis.Nil)
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	// проходим по списку geo_ордеров, вытягиваем id и получаем по нему структуру ордера из хранилища через метод GetByID
	orders = make([]models.Order, 0, len(ordersLocation))
	for _, orderLocation := range ordersLocation {
		orderId, err := strconv.ParseInt(orderLocation.Name, 10, 64)
		if err != nil {
			return nil, err
		}
		order, err = o.GetByID(ctx, orderId)
		if err != nil {
			return nil, err
		}
		if order != nil {
			orders = append(orders, *order)
		}
	}
	return orders, nil
}

func (o *OrderStorage) getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]redis.GeoLocation, error) {
	// создаем GeoRadiusQuery
	query := &redis.GeoRadiusQuery{
		Radius:      radius,
		Unit:        unit,
		WithCoord:   true,
		WithDist:    true,
		WithGeoHash: true,
	}
	// выполняем запрос к редису с помощью метода GeoRadius
	// "orders" - имя ключа представляющего индекс
	// lng, lat - координаты центра круга
	// query - параметры запроса
	orderLocation, err := o.storage.GeoRadius(ctx, "geo_orders", lng, lat, query).Result()
	if err != nil {
		return nil, fmt.Errorf("failed to get orders by radius from Redis: %v", err)
	}

	return orderLocation, nil
}

func (o *OrderStorage) GenerateUniqueID(ctx context.Context) (int64, error) {
	// используем INCR для увелечения значения ключа order:id
	id, err := o.storage.Incr(ctx, "order:id").Result()
	if err != nil {
		return 0, fmt.Errorf("failed to generate unique ID: %v", err)
	}

	return id, nil
}

func (o *OrderStorage) SetOrderDelivered(ctx context.Context, order *models.Order) error {
	order.IsDelivered = true
	err := o.Save(ctx, *order, 1*time.Millisecond)
	return err
}
