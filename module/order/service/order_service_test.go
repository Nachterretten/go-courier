package service

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/ptflp/geotask/geo"
	"gitlab.com/ptflp/geotask/module/order/models"
	"gitlab.com/ptflp/geotask/module/order/storage/mocks"
	"testing"
)

func TestOrderService_GetByRadius(t *testing.T) {
	// Создаем мок для OrderStorager
	storageMock := mocks.NewOrderStorager(t)

	// Создаем экземпляр OrderService с моком OrderStorager
	orderService := NewOrderService(storageMock, nil, nil)

	// Создаем тестовый контекст
	ctx := context.Background()

	// Создаем тестовые данные
	lng := 12.34
	lat := 56.78
	radius := 100.0
	unit := "km"

	// Устанавливаем ожидание вызова метода GetByRadius на моке OrderStorager
	// Проверяем, что метод будет вызван с правильными аргументами
	mockOrders := []models.Order{{ID: 1}, {ID: 2}}
	storageMock.On("GetByRadius", ctx, lng, lat, radius, unit).Return(mockOrders, nil)

	// Вызываем метод GetByRadius
	orders, err := orderService.GetByRadius(ctx, lng, lat, radius, unit)

	// Проверяем, что метод GetByRadius был вызван на моке OrderStorager
	storageMock.AssertCalled(t, "GetByRadius", ctx, lng, lat, radius, unit)

	// Проверяем, что возвращенные данные соответствуют ожидаемым
	assert.NoError(t, err)
	assert.Equal(t, mockOrders, orders)
}

func TestOrderService_Save(t *testing.T) {
	// Создаем мок для OrderStorager
	storageMock := mocks.NewOrderStorager(t)

	// Создаем экземпляр OrderService с моком OrderStorager
	orderService := NewOrderService(storageMock, nil, nil)

	// Создаем тестовый контекст
	ctx := context.Background()

	// Создаем тестовый заказ
	order := models.Order{ID: 1}

	// Устанавливаем ожидание вызова метода Save на моке OrderStorager
	// Проверяем, что метод будет вызван с правильными аргументами
	storageMock.On("Save", ctx, order, mock.AnythingOfType("time.Duration")).Return(nil)

	// Вызываем метод Save
	err := orderService.Save(ctx, order)

	// Проверяем, что метод Save был вызван на моке OrderStorager
	storageMock.AssertCalled(t, "Save", ctx, order, mock.AnythingOfType("time.Duration"))

	// Проверяем, что ошибки нет
	assert.NoError(t, err)
}

func TestOrderService_GetCount(t *testing.T) {
	// Создаем мок для OrderStorager
	storageMock := mocks.NewOrderStorager(t)

	// Создаем экземпляр OrderService с моком OrderStorager
	orderService := NewOrderService(storageMock, nil, nil)

	// Создаем тестовый контекст
	ctx := context.Background()

	// Устанавливаем ожидание вызова метода GetCount на моке OrderStorager
	// Проверяем, что метод будет вызван с правильными аргументами
	mockCount := int64(5)
	storageMock.On("GetCount", mock.Anything).Return(mockCount, nil)

	// Вызываем метод GetCount
	count, err := orderService.GetCount(ctx)

	// Проверяем, что метод GetCount был вызван на моке OrderStorager
	storageMock.AssertCalled(t, "GetCount", mock.Anything)

	// Проверяем, что возвращенные данные соответствуют ожидаемым
	assert.NoError(t, err)
	assert.Equal(t, mockCount, count)
}

func TestOrderService_RemoveOldOrders(t *testing.T) {
	// Создаем мок для OrderStorager
	storageMock := mocks.NewOrderStorager(t)

	// Создаем экземпляр OrderService с моком OrderStorager
	orderService := NewOrderService(storageMock, nil, nil)

	// создаем текстовый контекст
	ctx := context.Background()

	// Устанавливаем ожидание вызова метода RemoveOldOrders на моке OrderStorager
	// Проверяем, что метод будет вызван с правильными аргументами
	storageMock.On("RemoveOldOrders", ctx, mock.AnythingOfType("time.Duration")).Return(nil)

	// вызываем метод RemoveOldOrders
	err := orderService.RemoveOldOrders(ctx)

	// проверяем, что метод RemoveOldOrders был вызван на моке OrderStorager
	storageMock.AssertCalled(t, "RemoveOldOrders", ctx, mock.AnythingOfType("time.Duration"))

	// Проверяем, что ошибки нет
	assert.NoError(t, err)
}

func TestOrderService_GenerateOrder(t *testing.T) {

	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "GenerateOrder_test_ok",
			fields: fields{storage: mocks.NewOrderStorager(t),
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{context.Background()},
			wantErr: false,
		},

		{
			name: "GenerateOrder_test_error",
			fields: fields{storage: mocks.NewOrderStorager(t),
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{context.Background()},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.storage.
				On("GenerateUniqueID", mock.Anything).
				Return(func(ctx context.Context) (int64, error) {
					if tt.name == "GenerateOrder_test_error" {
						return int64(1), fmt.Errorf("")
					}
					return int64(0), nil
				})

			tt.fields.storage.
				On("Save", tt.args.ctx, mock.AnythingOfType("models.Order"), mock.Anything).
				Maybe().
				Return(nil)

			o := OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}

			if err := o.GenerateOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GenerateOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_SetOrderDelivered(t *testing.T) {
	// Создаем мок для OrderStorager
	storageMock := mocks.NewOrderStorager(t)

	// Создаем экземпляр OrderService с моком OrderStorager
	orderService := NewOrderService(storageMock, nil, nil)

	// Создаем тестовый контекст
	ctx := context.Background()

	// Создаем тестовый заказ
	order := &models.Order{ID: 1}

	// Устанавливаем ожидание вызова метода SetOrderDelivered на моке OrderStorager
	// Проверяем, что метод будет вызван с правильными аргументами
	storageMock.On("SetOrderDelivered", ctx, order).Return(nil)

	// Вызываем метод SetOrderDelivered
	err := orderService.SetOrderDelivered(ctx, order)

	// Проверяем, что метод SetOrderDelivered был вызван на моке OrderStorager
	storageMock.AssertCalled(t, "SetOrderDelivered", ctx, order)

	// Проверяем, что ошибки нет
	assert.NoError(t, err)
}
