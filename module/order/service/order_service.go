package service

import (
	"context"
	"fmt"
	"gitlab.com/ptflp/geotask/geo"
	"gitlab.com/ptflp/geotask/module/order/models"
	"gitlab.com/ptflp/geotask/module/order/storage"
	"math/rand"
	"time"
)

const (
	minDeliveryPrice = 100.00
	maxDeliveryPrice = 500.00

	maxOrderPrice = 3000.00
	minOrderPrice = 1000.00

	orderMaxAge = 2 * time.Minute
)

type Orderer interface {
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // возвращает заказы через метод storage.GetByRadius
	Save(ctx context.Context, order models.Order) error                                             // сохраняет заказ через метод storage.Save с заданным временем жизни OrderMaxAge
	GetCount(ctx context.Context) (int64, error)                                                    // возвращает количество заказов через метод storage.GetCount
	RemoveOldOrders(ctx context.Context) error                                                      // удаляет старые заказы через метод storage.RemoveOldOrders с заданным временем жизни OrderMaxAge
	GenerateOrder(ctx context.Context) error                                                        // генерирует заказ в случайной точке из разрешенной зоны, с уникальным id, ценой и ценой доставки
	SetOrderDelivered(ctx context.Context, order *models.Order) error
}

// OrderService реализация интерфейса Orderer
// в нем должны быть методы GetByRadius, Save, GetCount, RemoveOldOrders, GenerateOrder
// данный сервис отвечает за работу с заказами
type OrderService struct {
	storage       storage.OrderStorager
	allowedZone   geo.PolygonChecker
	disabledZones []geo.PolygonChecker
}

func NewOrderService(storage storage.OrderStorager, allowedZone geo.PolygonChecker, disallowedZone []geo.PolygonChecker) Orderer {
	return &OrderService{storage: storage, allowedZone: allowedZone, disabledZones: disallowedZone}
}

func (o OrderService) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	orders, err := o.storage.GetByRadius(ctx, lng, lat, radius, unit)
	if err != nil {
		return nil, fmt.Errorf("failed to get orders")
	}
	return orders, nil

}

func (o OrderService) Save(ctx context.Context, order models.Order) error {
	return o.storage.Save(ctx, order, orderMaxAge)
}

func (o OrderService) GetCount(ctx context.Context) (int64, error) {

	timeout := 10 * time.Millisecond

	ctxWithTimeout, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	count, err := o.storage.GetCount(ctxWithTimeout)
	if err != nil {
		return 0, fmt.Errorf("failed to get order count: %v", err)
	}

	return count, nil
}

func (o OrderService) RemoveOldOrders(ctx context.Context) error {

	return o.storage.RemoveOldOrders(ctx, orderMaxAge)
}

func (o OrderService) GenerateOrder(ctx context.Context) error {
	var order models.Order
	var err error

	order.ID, err = o.storage.GenerateUniqueID(ctx)
	if err != nil {
		return err
	}

	order.Price = minOrderPrice + rand.Float64()*(maxOrderPrice-minOrderPrice)
	order.DeliveryPrice = minDeliveryPrice + rand.Float64()*(maxDeliveryPrice-minDeliveryPrice)

	point := geo.GetRandomAllowedLocation(o.allowedZone, o.disabledZones)
	order.Lat, order.Lng = point.Lat, point.Lng
	order.CreatedAt = time.Now()

	return o.Save(ctx, order)
}

func (o OrderService) SetOrderDelivered(ctx context.Context, order *models.Order) error {
	return o.storage.SetOrderDelivered(ctx, order)
}
