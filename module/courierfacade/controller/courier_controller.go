package controller

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/ptflp/geotask/module/courierfacade/service"
	"net/http"
	"time"
)

type CourierController struct {
	courierService service.CourierFacer
	ctx            context.Context
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

// @Summary GetStatus
// @Tags Courier
// @Description get courier status
// @Accept json
// @Produce json
// @Param input body models.Courier
// @Success 200 {integer} integer 1
// @Failure 500 {object} errorResponse
// @Router /status [get]

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// получить статус курьера из сервиса courierService используя метод GetStatus
	time.Sleep(1 * time.Second)
	status, err := c.courierService.GetStatus(ctx)
	if err != nil {
		// если произошла 	ошибка при получении статуса курьера отправляем ошибку
		return
	}

	ctx.JSON(http.StatusOK, status)
}

func (c *CourierController) MoveCourier(m WebSocketMessage) {
	var cm CourierMove
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	data, ok := m.Data.([]byte)
	if !ok {
		//		log.Println("failed to load data")
		return
	}

	err := json.Unmarshal(data, &cm)
	if err != nil {
		//		log.Println("failed to unmarshalling")
		return
	}

	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
