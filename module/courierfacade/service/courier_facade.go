package service

import (
	"context"
	cservice "gitlab.com/ptflp/geotask/module/courier/service"
	cfm "gitlab.com/ptflp/geotask/module/courierfacade/models"
	oservice "gitlab.com/ptflp/geotask/module/order/service"
	"log"
)

const (
	CourierVisibilityRadius     = 2800 // 2.8km
	CourierDeliveredOrderRadius = 5    // 5m

)

//go:generate go run github.com/vektra/mockery/v2@v2.20.2 --name=CourierFacer
type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int)     // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) (cfm.CourierStatus, error) // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func (c *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Println("failed to get courier")
	}

	err = c.courierService.MoveCourier(ctx, *courier, direction, zoom)
	if err != nil {
		log.Println("failed to move courier")
	}
}

func (c *CourierFacade) GetStatus(ctx context.Context) (cfm.CourierStatus, error) {
	status := cfm.CourierStatus{}
	// получаем экземпляр куры
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Printf("getting status error: %s", err)
		return status, nil
	}
	// отправляем его в статус
	status.Courier = *courier

	// проверяем ордера на возможность доставки
	ordersForDelivery, err := c.orderService.GetByRadius(
		ctx,
		courier.Location.Lng,
		courier.Location.Lat,
		CourierDeliveredOrderRadius,
		"m",
	)
	if err != nil {
		log.Printf("set delivery order error, %v", err)
	}

	// если ордера есть, меняем их статус и время жизни
	if len(ordersForDelivery) != 0 {
		for _, order := range ordersForDelivery {
			err = c.orderService.SetOrderDelivered(ctx, &order)
			if err != nil {
				log.Printf("SetOrderDelivered error %v", err)
			}
			// добавляем стоимость ордера к score курьера
			courier.Score += int(order.DeliveryPrice)
			err := c.courierService.UpgradeCourier(ctx, *courier)
			if err != nil {
				return cfm.CourierStatus{}, err
			}
		}
	}

	// получаем ордера в радиусе видимости, отправляем их в статус
	status.Orders, err = c.orderService.GetByRadius(
		ctx,
		status.Courier.Location.Lng,
		status.Courier.Location.Lat,
		CourierVisibilityRadius,
		"m",
	)
	if err != nil {
		log.Printf("get status error,%v", err)
		return cfm.CourierStatus{}, nil
	}

	return status, nil
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}
