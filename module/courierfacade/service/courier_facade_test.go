package service

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ptflp/geotask/module/courierfacade/service/mocks"

	"gitlab.com/ptflp/geotask/module/courierfacade/models"
	"testing"
)

func TestMoveCourier(t *testing.T) {
	ctx := context.Background()

	// создаем экземпляр мок-объекта
	mockCourierFacer := mocks.NewCourierFacer(t)

	// ожидаемый вызов метода
	mockCourierFacer.On("MoveCourier", ctx, 1, 2)

	// вызываем метод MoveCourier
	mockCourierFacer.MoveCourier(ctx, 1, 2)

	// проверяем, что метод был вызван с ожидаемыми аргументами
	mockCourierFacer.AssertCalled(t, "MoveCourier", ctx, 1, 2)
}

func TestGetStatus(t *testing.T) {
	ctx := context.Background()

	// создаем экземпляр мок- объекта
	mockCourierFacer := mocks.NewCourierFacer(t)

	// ожидаемый вызов метода
	expectedStatus := models.CourierStatus{}
	mockCourierFacer.On("GetStatus", ctx).Return(expectedStatus, nil)

	// вызываем метод
	status, err := mockCourierFacer.GetStatus(ctx)

	// проверяем что метод был вызван с ожидаемыми аргументами
	mockCourierFacer.AssertCalled(t, "GetStatus", ctx)

	// проверяем что не было ошибок
	assert.NoError(t, err)

	// проверяем что возвращенный статус соответсвует ожидаемому
	assert.Equal(t, expectedStatus, status)
}
