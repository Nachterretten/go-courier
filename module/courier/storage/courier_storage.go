package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"gitlab.com/ptflp/geotask/module/courier/models"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.2 --name=CourierStorager
type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	storage *redis.Client
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{
		storage: storage,
	}
}

func (c CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	var data []byte
	// преобразуем куру в строку жсон
	data, err := json.Marshal(courier)
	if err != nil {
		return fmt.Errorf("failed to serialize")
	}

	// сохраняем данные куры в редисе
	return c.storage.Set(ctx, "courier", data, 0).Err()
}

func (c CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {

	var (
		data    []byte
		err     error
		courier models.Courier
	)
	// поулчаем данные куры из редиса
	data, err = c.storage.Get(ctx, "courier").Bytes()
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &courier)
	if err != nil {
		return nil, err
	}

	return &courier, nil
}
