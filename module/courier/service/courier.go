package service

import (
	"context"
	"fmt"
	"gitlab.com/ptflp/geotask/geo"
	"gitlab.com/ptflp/geotask/module/courier/models"
	"gitlab.com/ptflp/geotask/module/courier/storage"
	"math"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(ctx context.Context, courier models.Courier, direction, zoom int) error
	UpgradeCourier(ctx context.Context, courier models.Courier) error
}

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disbledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disbledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// получаем куру из репозитория
	courier, err := c.courierStorage.GetOne(ctx)
	if err != nil {
		// создаем новую куру
		courier = &models.Courier{
			Score:    0,
			Location: models.Point{Lat: DefaultCourierLat, Lng: DefaultCourierLng},
		}
		if err := c.courierStorage.Save(ctx, *courier); err != nil {
			return nil, err
		}
		return courier, nil
	}

	// проверим, где находится наша кура
	// если зона запрещена то перемещаем в разрешенную точку
	if !c.allowedZone.Contains(geo.Point(courier.Location)) {
		courier.Location = models.Point(c.allowedZone.RandomPoint())
	}

	// сохраняем новые координаты
	if err := c.courierStorage.Save(ctx, *courier); err != nil {
		return nil, err
	}
	return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(ctx context.Context, courier models.Courier, direction, zoom int) error {
	// точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
	// 14 - это максимальный зум карты
	delta := 0.001 / math.Pow(2, float64(zoom-14))
	switch direction {
	case DirectionUp:
		courier.Location.Lat += delta
	case DirectionDown:
		courier.Location.Lat -= delta
	case DirectionLeft:
		courier.Location.Lng -= delta
	case DirectionRight:
		courier.Location.Lng += delta
	default:
		return fmt.Errorf("direction: %d has not recognized", direction)
	}

	// далее нужно проверить, что курьер не вышел за границы зоны
	// если вышел, то нужно переместить его в случайную точку внутри зоны
	point := geo.Point{}
	point.Lat, point.Lng = courier.Location.Lat, courier.Location.Lng
	if !geo.CheckPointIsAllowed(point, c.allowedZone, c.disabledZones) {
		point = geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location.Lat, courier.Location.Lng = point.Lat, point.Lng
	}

	// далее сохранить изменения в хранилище
	err := c.courierStorage.Save(ctx, courier)
	if err != nil {
		return err
	}

	return nil
}

func (c *CourierService) UpgradeCourier(ctx context.Context, courier models.Courier) error {
	return c.courierStorage.Save(ctx, courier)
}
