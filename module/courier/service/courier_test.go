package service

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/mock"
	"gitlab.com/ptflp/geotask/geo"
	"gitlab.com/ptflp/geotask/module/courier/models"
	"gitlab.com/ptflp/geotask/module/courier/storage/mocks"
	"reflect"
	"testing"
)

func TestCourierService_GetCourier(t *testing.T) {
	type fields struct {
		courierStorage *mocks.CourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.Courier
		wantErr bool
	}{
		{
			name: "GetCourier_test",
			fields: fields{
				courierStorage: mocks.NewCourierStorager(t),
				allowedZone:    geo.NewAllowedZone(),
				disabledZones:  []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{ctx: context.Background()},
			want:    &models.Courier{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierStorage.On("GetOne", tt.args.ctx).Return(tt.want, nil)
			tt.fields.courierStorage.On("Save", tt.args.ctx, mock.AnythingOfType("models.Courier")).Return(nil)
			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			got, err := c.GetCourier(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCourier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourier() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierService_MoveCourier(t *testing.T) {

	type fields struct {
		courierStorage *mocks.CourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		courier   models.Courier
		direction int
		zoom      int
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "MoveCourier_test",
			fields: fields{
				courierStorage: mocks.NewCourierStorager(t),
				allowedZone:    geo.NewAllowedZone(),
				disabledZones:  []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args: args{
				courier:   models.Courier{},
				direction: 0,
				zoom:      0,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.courierStorage.
				On("Save", context.Background(), mock.AnythingOfType("models.Courier")).
				Return(nil)

			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}

			if err := c.MoveCourier(context.Background(), tt.args.courier, tt.args.direction, tt.args.zoom); (err != nil) != tt.wantErr {
				t.Errorf("MoveCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCourierService_UpgradeCourier(t *testing.T) {

	type fields struct {
		courierStorage *mocks.CourierStorager
		allowedZone    geo.PolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx     context.Context
		courier models.Courier
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Save_test_ok",
			fields: fields{
				courierStorage: mocks.NewCourierStorager(t),
				allowedZone:    geo.NewAllowedZone(),
				disabledZones:  []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args: args{
				ctx:     context.Background(),
				courier: models.Courier{},
			},
			wantErr: false,
		},

		{
			name: "Save_test_error",
			fields: fields{
				courierStorage: mocks.NewCourierStorager(t),
				allowedZone:    geo.NewAllowedZone(),
				disabledZones:  []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args: args{
				ctx:     context.Background(),
				courier: models.Courier{},
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.courierStorage.
				On("Save", tt.args.ctx, tt.args.courier).
				Return(func(ctx context.Context, courier models.Courier) error {
					if tt.name == "Save_test_error" {
						return fmt.Errorf("")
					}
					return nil
				})

			c := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}

			if err := c.UpgradeCourier(tt.args.ctx, tt.args.courier); (err != nil) != tt.wantErr {
				t.Errorf("UpgradeCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
