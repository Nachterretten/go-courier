package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/ptflp/geotask/module/courierfacade/controller"
)

type Router struct {
	courier *controller.CourierController
}

func NewRouter(courier *controller.CourierController) *Router {
	return &Router{courier: courier}
}

func (r *Router) CourierAPI(router *gin.RouterGroup) {
	router.GET("/ws", r.courier.Websocket)
	router.GET("/status", r.courier.GetStatus)
}

func (r *Router) Swagger(router *gin.RouterGroup) {
	router.GET("/swagger", swaggerUI)
}

/*func (r *Router) MoveCourierHandler(ctx *gin.Context) {
	var m controller.WebSocketMessage
	err := ctx.ShouldBindJSON(&m)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	r.courier.MoveCourier(m)

	ctx.JSON(http.StatusOK, gin.H{"message": "Courier moved successfully"})
}*/
