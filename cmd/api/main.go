package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/ptflp/geotask/run"
	"log"
	"os"
)

// @title Courier
// @version 1.0
// @description Courier game

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("failed to load .env file:%s", err)
	}
	// инициализация приложения
	app := run.NewApp()
	// запуск приложения
	err = app.Run()
	// в случае ошибки выводим ее в лог и завершаем работу с кодом 2
	if err != nil {
		log.Println(fmt.Sprintf("error: %s", err))
		os.Exit(2)
	}
}
